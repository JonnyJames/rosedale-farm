<?php 

/*Template Name: Contact */

get_header(); ?>
		<!-- section -->
		<section class="contact">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>

			<?php endwhile; else : ?>
			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
		</section>
		<!-- /section -->


<?php get_sidebar(); ?>

<?php get_footer(); ?>