<?php

/* Template Name: About */ 

get_header(); ?>

		<div class="hero">
			<?php
	 		if ( has_post_thumbnail() ) {
	 		the_post_thumbnail("full");
			}
			?>
		</div>
		<div class="about main">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<h2><?php the_title(); ?></h2>
		<p><?php the_content(); ?></p>

			<?php endwhile; else : ?>
			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>

	</div>
<?php get_sidebar(); ?>

<?php get_footer(); ?>
