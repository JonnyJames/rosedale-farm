<?php 

/* Template Name: Home */ 

get_header(); ?>
		<div class="hero">
			<?php
	 		if ( has_post_thumbnail() ) {
	 		the_post_thumbnail("full");
			}
			?>
		</div>
		<!-- site content -->
		<section class="site-content main">		
		<!-- main-column -->
				<div class="posts">
				<h1>Rosedale Farm in the News</h1>
			    <?php
					$args = array( 'posts_per_page' => 3 );
					$lastposts = get_posts( $args );
					foreach ( $lastposts as $post ) :
					  setup_postdata( $post ); ?>
						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<?php the_content(); ?>	
					<?php endforeach; 
					wp_reset_postdata(); ?>
			    </div>
		</section>		
<?php get_sidebar(); ?>

<?php get_footer(); ?>

		</section>
		<!-- /section -->