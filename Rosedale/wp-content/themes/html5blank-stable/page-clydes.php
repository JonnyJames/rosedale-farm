<?php

/*Template Name: Clydesdale Gallery */

get_header(); ?>	
		<div class="hero">
			<?php
	 		if ( has_post_thumbnail() ) {
	 		the_post_thumbnail('full');
			}
			?>
		</div>
		<!-- this is the start of the gallery section/wrapper -->
		<section class="clydes">
				<?php $horse = new WP_Query(
					array(
						'posts_per_page' => 8,
						'post_type' => 'horse'
						)		
					);?>	
				<?php if ($horse->have_posts() ) : ?>	
				<?php while ($horse->have_posts()) : $horse->the_post(); ?>
				<div class="horse">
					<?php the_content() ?>
					<?php get_post_custom('medium') ?>
					
					<?php the_meta(); ?>
				</div>
				<?php endwhile; endif; ?>
				<?php wp_reset_postdata(); ?>

		</section>
		<!-- /section -->	


<?php get_footer(); ?>
