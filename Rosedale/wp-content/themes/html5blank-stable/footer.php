			<!-- footer -->
			<div class="social">
				<div class="footWidget">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer_left') ) : ?>
					<?php endif; ?>
			<!-- copyright -->
			<footer class="footer" role="contentinfo">
				<p class="copyright">
					<p>Design & Development by Jonny James
					&copy; <?php echo date('Y'); ?></p>
					<!-- <a href="//wordpress.org" title="WordPress">WordPress</a> &amp; <a href="//html5blank.com" title="HTML5 Blank">HTML5 Blank</a>. -->
				</p>
				<!-- /copyright -->
			</footer>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>
		<!-- jQuery library (served from Google) -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<!-- bxSlider Javascript file -->
		<script src="/js/jquery.bxslider.min.js"></script>
		<script src="/js/jquery.easing.1.3.js"></script>
		<script src="/js/jquery.fitvids.js"></script>
		<!-- bx slider controls -->
		<script type="text/javascript">
		jQuery(document).ready(function(){
		  jQuery('bxslider').bxSlider({
		    mode: 'horizontal',
		    infiniteLoop: true,
		    speed: 2000,
		    pause: 8000,
		    auto: true,
		    pager: false,
		    controls: true
		});
		});
		</script>
		<!-- end of bxslider gallery -->
	
	</body>
</html>
