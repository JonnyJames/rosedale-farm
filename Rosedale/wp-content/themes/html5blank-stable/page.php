<?php get_header(); ?>
		

		<!-- this is the Contact Page -->
		<section class="gallery clearfix">
				<div class="full">
		      	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		      	<!-- <h2><?php the_title(); ?></h2> -->
				<p class="name"><?php the_field('name'); ?></p> 

				<div class="breed">
				  <?php the_field('breed'); ?>
				</div>
				
				<div class="height">
				  <?php the_field('height'); ?>
				</div>

				<?php the_content(); ?>

	
        
      <?php endwhile; // end of the loop. ?>
    </div>
		</section>
		<!-- /section -->

<?php get_footer(); ?>
