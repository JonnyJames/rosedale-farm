<?php get_header(); ?>
		<!-- section -->
		<section class="main">
			<h1>News from Rosedale Farm</h1>
			<p>This is the index.php and this is where the meat & potatoes go</p>
			<h1>Clydesdale Gallery</h1>
			<p>this is index.php!!</p>
				<?php $horses = new WP_Query(
					array(
						'posts_per_page' => 10,
						'post_type' => 'horses'
						)		
					);?>	
				<?php if ($horses->have_posts() ) : ?>	
				<?php while ($horses->have_posts()) : $horses->the_post(); ?>
				<div class="horses">
					<h1><?php the_title() ?></h1>
					<?php the_content() ?>
					<?php the_post_thumbnail( 'square' ); ?>
				</div>
				<?php endwhile; endif; ?>
				<?php wp_reset_postdata(); ?>
		</section>
		<!-- /section -->


<?php get_sidebar(); ?>

<?php get_footer(); ?>
